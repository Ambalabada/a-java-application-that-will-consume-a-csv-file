package com.artiom.csvapplication.data;

import com.artiom.csvapplication.Person;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

public class SQLOperations {

    private static final Logger LOGGER = LogManager.getLogger(SQLOperations.class);

    public void performSQLiteOperations(List<Person> people) {
        Connection connection = establishConnection();
        createTable(connection);
        populateSQLiteTable(connection, people);
    }

    private Connection establishConnection (){
        Connection connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite::memory:");
        }catch ( Exception e) {
            LOGGER.fatal("Connection could not be established for some reason, shutting down", e);
            System.exit(-1);
        }
        return connection;
    }

    private void createTable(Connection connection){
        try {
            Statement stat = connection.createStatement();

            stat.executeUpdate("drop table if exists people");

            stat.executeUpdate("create table people(A NVARCHAR,"
                    + "B NVARCHAR," + "C NVARCHAR," + "D NVARCHAR,"
                    + "E TEXT," + "F NVARCHAR," + "G NVARCHAR,"
                    + "H NVARCHAR," + "I NVARCHAR," + "J NVARCHAR);");
        } catch ( SQLException e ) {
            LOGGER.fatal("Table could not be created for some reason, shutting down", e);
            System.exit(-1);
        }
    }

    private void populateSQLiteTable(Connection connection, List<Person> people){
        String sql = "INSERT INTO people(A,B,C,D,E,F,G,H,I,J) VALUES(?,?,?,?,?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){

            for (Person person: people) {
                preparedStatement.setString(1, person.getA());
                preparedStatement.setString(2, person.getB());
                preparedStatement.setString(3, person.getC());
                preparedStatement.setString(4, person.getD());
                preparedStatement.setString(5, person.getE());
                preparedStatement.setString(6, person.getF());
                preparedStatement.setString(7, person.getG());
                preparedStatement.setString(8, person.getH());
                preparedStatement.setString(9, person.getI());
                preparedStatement.setString(10, person.getJ());
                preparedStatement.executeUpdate();
            }
            preparedStatement.close();

            connection.close();

        } catch ( SQLException e ) {
            LOGGER.fatal("Table could not be populated for some reason, shutting down", e);
            System.exit(-1);
        }
    }
}