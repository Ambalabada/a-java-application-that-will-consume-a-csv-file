package com.artiom.csvapplication;

import com.artiom.csvapplication.csvservice.CSVProcessingService;

public class AppEntryPoint {
    public static void main(String[] args ){
        new CSVProcessingService().processCSVFile();
    }
}
