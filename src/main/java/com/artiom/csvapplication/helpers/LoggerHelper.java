package com.artiom.csvapplication.helpers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LoggerHelper {

    private static final Logger LOGGER = LogManager.getLogger(LoggerHelper.class);

    public static void logResultsForCSVRecords(Integer recordsFailed, Integer recordsSuccessful){

        /*
        Headers are not counted, because they are not records
        */
        LOGGER.info(recordsFailed + recordsSuccessful + "  of records received");
        LOGGER.info(recordsSuccessful + "  of records successful");
        LOGGER.info(recordsFailed + " of records failed");
    }
}
