package com.artiom.csvapplication.csvservice;

import com.artiom.csvapplication.helpers.LoggerHelper;
import com.artiom.csvapplication.Person;
import com.artiom.csvapplication.data.SQLOperations;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.*;


public class CSVProcessingService {

    private static final Logger LOGGER = LogManager.getLogger(CSVProcessingService.class);


    @SuppressWarnings({"unchecked"})
    public void processCSVFile() {
        Properties properties = getProperties();

        Map<String,List> resultMap = readFileAndGetParsedCSV(properties);

        new SQLOperations().performSQLiteOperations(resultMap.get("parsedCSV"));

        setBadDataProperty(properties);

        Integer numberOfFailedRecords = writeBadDataToCSV(properties, resultMap.get("exceptions"));

        LoggerHelper.logResultsForCSVRecords(numberOfFailedRecords,resultMap.get("parsedCSV").size());

        }

    @SuppressWarnings({"unchecked"})
    private Map<String, List> readFileAndGetParsedCSV(Properties properties){
        try(CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader(properties.getProperty("csv.file"))))) {
            Map<String, List> returnMap = new HashMap<>();
            CsvToBean<Person> csvToBean = new CsvToBeanBuilder(csvReader).withType(Person.class).withThrowExceptions(false).build();
            returnMap.put("parsedCSV", csvToBean.parse());
            returnMap.put("exceptions", csvToBean.getCapturedExceptions());
            return returnMap;
        } catch (IOException e){
            LOGGER.error("Could not find csv file to read. Provide one and write it's location in properties.xml. Shutting down", e);
            System.exit(-1);
            return null;
        }
    }

    private Integer writeBadDataToCSV(Properties properties, List<CsvException> csvExceptions) {
        final List<String[]> badData = new ArrayList<>();

        Path path = Paths.get(properties.getProperty("bad.data.file"));
        try {
            Files.createFile(path);
        }catch (IOException e){
            LOGGER.fatal("Could not create bad-data-<timestamp> file, check file creation permissions", e);
            System.exit(-1);
        }

        if (Files.exists(path)) {

            try (CSVWriter writer = new CSVWriter(new BufferedWriter(new FileWriter(properties.getProperty("bad.data.file"))))) {
                for (CsvException exception : csvExceptions) {
                    badData.add(exception.getLine());
                }
                /*
                **First commit**
                There is a small problem. File contains 6002 lines, but for some reason openCSV adds one blank line after reading and thus
                I get 6003 rows with the last as "". The last one gives exception, because it does not match column headers.
                This is a bug of newer versions, but I still used it at this work because it has some handful methods. So don't mind this if

                **Second commit.**

                This was what I was thinking at first, but later I got really curios about it and found out that
                Interview-task-data-osh (2) actually contains extra character at the end.

                 Last two characters in this file are 0x0a 0x0a. The first 0x0a is to end the last real record in the file, line number 6001.
                 The second line feed is extraneous and that's why it is actually not a bug, but my mistake at the beginning.
                 File actually contains 6003 rows.
                */
                if (badData.size() > 1 && StringUtils.isEmpty(badData.get(badData.size() - 1)[0])) {
                    badData.get(badData.size() -1)[0] = " 'Warning last line contains 0x0a character, which creates blank line in file'" ;
                }

                writer.writeAll(badData, false);
            } catch (IOException e){
                LOGGER.fatal("Could not retrieve file path from properties. Shutting down", e);
                System.exit(-1);

            }
        } else {
            LOGGER.fatal("File was not created or system has no rights to read files");
            System.exit(-1);
        }
        return badData.size();
    }

    private Properties getProperties()  {
        Properties properties = new Properties();
        try {
            properties.loadFromXML(new BufferedInputStream(new FileInputStream(Thread.currentThread().getContextClassLoader().getResource("").getPath() + "properties.xml")));
        } catch (IOException e) {
            LOGGER.fatal("Could not load properties from xml file, make sure properties.xml are in classpath and system has rights to read/write. Shutting down", e);
            System.exit(-1);
        }
        return properties;
    }

    private void setBadDataProperty(Properties properties){
        properties.setProperty("bad.data.file", "bad-data-" + new Timestamp((System.currentTimeMillis())).toString().replaceAll(":", "꞉") + ".csv");
    }


}
