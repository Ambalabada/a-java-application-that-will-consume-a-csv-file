# README #

A Java application that will consume a CSV file. I've done it by using OpenCSV, JDBC, SQLite, Log4j2, Maven.

In order to keep it in KISS and do not overkill with Spring and etc I decided to stick with JDBC and familiar from previous work OpenCSV.
If you expected that I would use other technologies or libraries let me know about it so we can discuss it.  

### How to run? ###

First of all you need Java installed on you machine. Oracle can help with that

As it was not necessary I did not provide nor jar nor GUI to interact with. But if you would like to see this application with one I can easily provide both of them.

That's why you will have to start application from your IDE or command line. I've used Intellij IDEA during my working process. 
Application's main method is inside AppEntryPoint.class

After you download this application make sure you have CSV file in root folder, where pom.xml with readme resides.
I've included one which I was given to work to, it's name is "Interview-task-data-osh (2).csv". 

If you want to use another one with different name, make sure you will change name of entry key "csv.file" in properties.xml. properties.xml file is in "resources" folder. 

<entry key="csv.file">Interview-task-data-osh (2).csv</entry> This is line in which you will have to change "Interview-task-data-osh (2).csv" on your CSV file name.

That's basically it. As was needed, SQLite is in-memory mode, so you wont have to do any configurations.

# Where is output or bad-data? Where are logs? Why applications exits with -1? #

bad-data-<timestamp>.csv will be generated in the root folder of project near your CSV file and pom.xml.

Logs folder will be generated after first start of application in java-application folder and two files will be inside: statistics.log and errors.log. They contains all logs and wont override themselves.

If everything goes well you will find statistic about records in statistics.log and in console of your IDE will be written "Process finished with exit code 0"

If something won't go well you will find error cause and error stack trace up there and in console of your IDE will be written "Process finished with exit code -1"

Let me know if you have errors and cant run this app.